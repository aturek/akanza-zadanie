**Testing**

Testing procedure for this specific task covers latest version of Chrome with following screen widths: 1280px, 768px, 640px, 320px.

## Workflow

1. npm install --save-dev gulp node-sass gulp-sass gulp-cssnano
2. Start main gulp task - `gulp watch` - it will compile SCSS automatically after each change