(function ($) {

    
    function SingleLetterNbsp() {
        var elements = 'p, h1, h2, h3, h4, h5, h6, a, span';
        $(elements).each(function () {
          var letters = ["a", "A", "i", "I", "o", "O", "u", "U", "w", "W", "z", "Z", "e", "E", "s", "S", "v", "V"];
          for (var i = 0, counter = letters.length; i < counter; i++) {
            var letter = (letters[i]);
            $(this).html($(this).html().replace(new RegExp(' ' + letter + ' ', 'g'), ' ' + letter + '&nbsp;'));
          };
        });
      };

    $(window).on("load resize ", function () {
        SingleLetterNbsp();
      });
    
})(jQuery);